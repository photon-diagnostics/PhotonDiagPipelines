import epics as ep
import numpy as np
from time import sleep
from collections import deque

PMOS_spectrum_name = 'SATOP31-PMOS132-2D:SPECTRUM_Y'
PMOS_e_axis_name = 'SATOP31-PMOS132-2D:SPECTRUM_X'

PMOS_auto_name = 'SATOP31-PMOS132-2D:AUTO'
PMOS_lags_name = 'SATOP31-PMOS132-2D:LAGS'


PMOS_spectrum_PV = ep.PV(PMOS_spectrum_name)
PMOS_e_axis_PV = ep.PV(PMOS_e_axis_name)

PMOS_auto_PV = ep.PV(PMOS_auto_name)
PMOS_lags_PV = ep.PV(PMOS_lags_name)
freq = ep.caget("SWISSFEL-STATUS:Bunch-2-Appl-Freq-RB")

def grab_spec(NumShots):
        
    Spectrum = deque(maxlen = NumShots)
    def on_value_change(value=None,pv = None, **kwargs):
        Spectrum.append(value)

        if len(Spectrum) == NumShots:
            pv.clear_callbacks()

    PMOS_spectrum_PV.add_callback(callback=on_value_change, pv =PMOS_spectrum_PV)
    while len(Spectrum) < NumShots:
        sleep(1/freq)
    e_axis = PMOS_e_axis_PV.get()

    return(np.array(e_axis),np.array(Spectrum))

def auto_corr(e_axis, Spec):
    correlations = np.zeros_like(Spec)
    correlations = []
    
    for i in range(0,Spec.shape[0]):
        test = np.correlate(Spec[i,:].astype('float'),Spec[i,:].astype('float'), mode='same')
        
        correlations.append(test)
    lags = e_axis - e_axis[int(e_axis.size /2)]
    correlations = np.asarray(correlations)
    return lags, correlations.mean(axis=0)

while True:
    NumShots = int(ep.caget('SATOP31-PMOS132-2D:AVG_NUM'))
    e_axis, spec = grab_spec(NumShots)
    lags, mean_cor = auto_corr(e_axis, spec)
    sig = mean_cor/np.max(mean_cor)
    PMOS_auto_PV.put(sig)
    PMOS_lags_PV.put(lags)
